# Proyecto ClaseEjemplo

Se agrega clase de ejemplo usada para mostrar como usar DAO, Models y Controllers.

## Instalación
Para instalar las librerias en el IDE Eclipse debes ir al proyecto y hacer clic con el boton derecho, luego seleccionar Properties >> Java Build Path >> Libreries >> Classpath >> boton Add External JARs, y agregar las librerias (jar) que necesitas.

## Clonar el proyecto
Para clonar el repositorio, utiliza el siguiente comando:

```bash
git clone https://gitlab.com/202405-alkemy-java-6/202405-115-116-claseejemplo.git

