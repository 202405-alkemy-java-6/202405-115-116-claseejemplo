package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.Producto;

public class Dao {
	
    private Connection conexion;

    public Dao(Connection conexion) {
        this.conexion = conexion;
    };
     
    public Producto realizarConsulta() {
    	
    String consultaSQL = "SELECT id, name, price FROM product";
    try {
        Statement statement = conexion.createStatement();
        ResultSet resultSet = statement.executeQuery(consultaSQL);

         
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String nombre = resultSet.getString("name");
            String precio = resultSet.getString("price");

            System.out.println("ID: " + id + ", name: " + nombre + ", price: " + precio);
        }
        resultSet.close();
        statement.close();
    } catch (SQLException e) {
        System.err.println("Error al ejecutar la consulta: " + e.getMessage());
    }
	return null;
    }
    
   /* public Producto consultarUsuarioPorId(int id) {
        String consulta = "SELECT id, name, price FROM product WHERE id = ?";
        try {
        	PreparedStatement ps = conexion.prepareStatement(consulta);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
            	// return new Producto(rs.getInt("id"), rs.getString("name"), rs.getString("price"));
            	System.out.println( new Producto(rs.getInt("id"), rs.getString("name"), rs.getString("price")));
            }
        } catch (SQLException e) {
            System.err.println("Error al consultar el Producto: " + e.getMessage());
        }
        return null;
    }*/
    
    public void insertarProducto(Producto producto) {
    	  String consulta = "INSERT INTO product (name, price) VALUES (?, ?)";
          try {
        	  PreparedStatement ps = conexion.prepareStatement(consulta);
              ps.setString(1, producto.getName());
              ps.setString(2, producto.getPrice());
              ps.executeUpdate();
              System.out.println("Producto creado correctamente.");
          } catch (SQLException e) {
              System.err.println("Error al crear el Producto: " + e.getMessage());
          }
    }
    public void actualizarProducto(Producto producto) {
        String consulta = "UPDATE product SET name = ?, price = ? WHERE id = ?";
        try
        {
        	PreparedStatement ps = conexion.prepareStatement(consulta);
        	ps.setString(1, producto.getName());
            ps.setString(2, producto.getPrice());
            ps.setInt(3, producto.getId());
            ps.executeUpdate();
            System.out.println("Producto actualizado correctamente.");
        } catch (SQLException e) {
            System.err.println("Error al actualizar el Producto: " + e.getMessage());
        }
    }

    public void eliminarProducto(int id) {
    	  String consulta = "DELETE FROM product WHERE id = ?";
          try {
        	  PreparedStatement ps = conexion.prepareStatement(consulta);
              ps.setInt(1, id);
              ps.executeUpdate();
              System.out.println("Producto eliminado correctamente.");
          } catch (SQLException e) {
              System.err.println("Error al eliminar el Producto: " + e.getMessage());
          }
    }
}

